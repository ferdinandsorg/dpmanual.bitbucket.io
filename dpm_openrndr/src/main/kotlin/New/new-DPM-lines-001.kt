package New

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.math.Vector2
import org.openrndr.math.map
import org.openrndr.shape.contour

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.stroke = ColorRGBa.PINK
            drawer.fill = null

            val num = map(0.0, width.toDouble(), 1.0, 75.0, mouse.position.x).coerceAtLeast(1.0).coerceAtMost(75.0).toInt()
            val thickness = map(0.0, height.toDouble(), 1.0, 5.0, mouse.position.y).coerceAtLeast(1.0).coerceAtMost(5.0)

            drawer.strokeWeight = thickness

            for (i in 0 .. num) {
                val linePositionX = map(0.0, num.toDouble(), 10.0, width - 10.0, i.toDouble())
                val c = contour {
                    moveTo(linePositionX,0.0)
                    curveTo(mouse.position, mouse.position, Vector2(linePositionX, height.toDouble()))
                }
                drawer.contour(c)
            }
        }
    }
}