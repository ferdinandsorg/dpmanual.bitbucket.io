package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extra.noise.valueLinear
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var randSeed = 1
        var gridStep = 55
        var lineLen = 50
        var lineDensity = 10
        val strokeWidth = 1.5

        keyboard.character.listen {
            if(it.character == 'r') { randSeed = (Math.random() * 1000).toInt() }
            if(it.character == '+') {gridStep+=5; lineLen+=5}
            if(it.character == '-') if(gridStep>5){gridStep-=5;  lineLen-=5}
            if(it.character == 'w') lineLen +=5
            if(it.character == 'x') if(lineLen>5){lineLen -=5}
            if(it.character == 'l') lineDensity +=1
            if(it.character == 'm') if(lineDensity>1){lineDensity -= 1}
        }

        fun drawLines(x: Double, y:Double, len: Int, isVertical:Boolean) {
            val inter = gridStep/lineDensity

            for (i in 0 until gridStep step inter) {
                if (isVertical) {
                    drawer.lineSegment(x+i, y-len, x+i, y+len)
                } else {
                    drawer.lineSegment(x-len, y+i, x+len, y+i)
                }
            }
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.stroke = ColorRGBa.WHITE
            drawer.strokeWeight = strokeWidth

            for (y in 0 until height step gridStep) {
                for (x in 0 until width step gridStep) {

                    val randVal = valueLinear(randSeed, x.toDouble(), y.toDouble()) * 12.0 + 12.0
                    val cutOffValX = map(0.0, width.toDouble(), 0.0, 12.0, mouse.position.x)
                    val cutOffValY = map(0.0, height.toDouble(), 0.0, 12.0, mouse.position.y)

                    if (randVal <= cutOffValX) {
                        drawLines(x.toDouble(), y.toDouble(), lineLen, true);
                    }
                    if (randVal > cutOffValY) {
                        drawLines(x.toDouble(), y.toDouble(), lineLen, false);
                    }
                }
            }
        }
    }
}