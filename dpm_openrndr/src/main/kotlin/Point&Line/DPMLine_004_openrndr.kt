package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extra.noise.valueLinear
import org.openrndr.math.map

// activate "orx-noise" in build.grtadle.kts

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        val lenMax = 180
        var randSeed = 1

        keyboard.character.listen {
            if (it.character == 'r') { randSeed = (Math.random() * 1000).toInt() }
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.stroke = ColorRGBa.WHITE
            drawer.translate(width/2.0, height/2.0)

            val numRepeats = map(0.0, width.toDouble(), 5.0, 150.0, mouse.position.x).coerceAtLeast(5.0).coerceAtMost(150.0)
            val thickness = map(0.0, height.toDouble(), 0.25, 25.0, mouse.position.y)
            drawer.strokeWeight = thickness
            val angleStep = 360.0 / numRepeats
            var count = 0.0

            for(i in 0 until 360) {
                val degree = count

                if(degree < 360) {
                    val len = (valueLinear(randSeed, i.toDouble(), 0.0) * 0.5 + 0.5) * lenMax
                    val x = Math.cos(Math.toRadians(degree)) * 50.0
                    val y = Math.sin(Math.toRadians(degree)) * 50.0
                    val x2 = Math.cos(Math.toRadians(degree)) * (50 + len)
                    val y2 = Math.sin(Math.toRadians(degree)) * (50 + len)
                    drawer.lineSegment(x, y, x2, y2)
                }
                count += angleStep
            }
        }
    }
}