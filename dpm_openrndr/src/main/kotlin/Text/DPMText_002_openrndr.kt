package Text

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.loadFont
import org.openrndr.extra.noise.valueLinear
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var randSeed = 1
        var charSize = 52.0

        var font = loadFont("dpm_openrndr/data/fonts/FiraCode-Bold.otf", charSize)

        keyboard.character.listen {
            if (it.character == 'r') { randSeed = (Math.random() * 1000).toInt() }
            if (it.character == '+') {
                charSize++
                font = loadFont("dpm_openrndr/data/fonts/FiraCode-Bold.otf", charSize)
            }
            if (it.character == '-') {
                println(charSize)
                if (charSize>1) {
                    charSize--
                    font = loadFont("dpm_openrndr/data/fonts/FiraCode-Bold.otf", charSize)
                }
            }
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null
            drawer.fontMap = font

            val xMax = 500 / charSize.toInt()
            val yMax = 500 / charSize.toInt()

            val glyphMin = map(0.0, width.toDouble(), 33.0, 126.0, mouse.position.x).coerceIn(33.0, 126.0).toInt()
            val glyphMax = map(0.0, height.toDouble(), 33.0, 126.0, mouse.position.y).coerceIn(33.0, 126.0).toInt()

            for (y in 1 until yMax) {
                for (x in 1 until xMax) {

                    val range = glyphMax - glyphMin
                    val randVal = glyphMin + (valueLinear(randSeed, x.toDouble(), y.toDouble()) * 0.5 + 0.5) * range

                    val c = randVal.toChar().toString()

                    val xx = x * charSize
                    val yy = y * charSize

                    drawer.text(c, xx, yy)
                }
            }
        }
    }
}
