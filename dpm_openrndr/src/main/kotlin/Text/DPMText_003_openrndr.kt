package Text

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolated
import org.openrndr.draw.loadFont
import org.openrndr.extra.noise.valueLinear
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var randSeed = 1
        var charSize = 18.0
        var isSimpleRand = true
        val glyphGridSize = 8
        var gridGap = charSize

        var font = loadFont("dpm_openrndr/data/fonts/FiraCode-Bold.otf", charSize)

        keyboard.character.listen {
            if (it.character == 'r') { randSeed = (Math.random() * 1000).toInt() }
            if (it.character == '+') {
                charSize++
                font = loadFont("dpm_openrndr/data/fonts/FiraCode-Bold.otf", charSize)
            }
            if (it.character == '-') {
                if (charSize>1) {
                    charSize--
                    font = loadFont("dpm_openrndr/data/fonts/FiraCode-Bold.otf", charSize)
                }
            }
            if (it.character == 'a') { isSimpleRand = !isSimpleRand }
            if (it.character == 'w') { gridGap++ }
            if (it.character == 'x') { if (gridGap>1) { gridGap-- } }
        }

        fun drawLetters(x: Int, y: Int, num:Int) {
            drawer.isolated {
                drawer.translate(x.toDouble(), y.toDouble())
                val randMin = map(0.0, width.toDouble(), 33.0, 45.0, mouse.position.x).toInt()
                val randMax = map(0.0, height.toDouble(), 46.0, 126.0, mouse.position.y).toInt()

                var c = ""

                if (isSimpleRand) {
                    val range = randMax - randMin
                    val randVal = randMin + (valueLinear(randSeed, x.toDouble(), y.toDouble()) * 0.5 + 0.5) * range
                    c = randVal.toChar().toString()
                }

                for (y in 0 until num) {
                    for (x in 0 until num) {
                        if (!isSimpleRand) {
                            val range = randMax - randMin
                            val randVal = randMin + (valueLinear(randSeed, x.toDouble(), y.toDouble()) * 0.5 + 0.5) * range
                            c = randVal.toInt().toChar().toString()
                        }
                        val xx = x * charSize
                        val yy = y * charSize
                        drawer.text(c, xx, yy)
                    }
                }
            }
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null
            drawer.fontMap = font

            val interval = (charSize * glyphGridSize + gridGap).toInt()

            for (y in 20 until height-15 step interval) {
                for (x in 15 until width-15 step interval) {
                    drawLetters(x, y, glyphGridSize)
                }
            }
        }
    }
}
