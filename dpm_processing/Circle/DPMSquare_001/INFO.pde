/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMSquare_001
 * Parent Sketch: DPMCircle_002
 * Type: static
 *
 * Summary : Concentric squares
 *
 * GIT:
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */