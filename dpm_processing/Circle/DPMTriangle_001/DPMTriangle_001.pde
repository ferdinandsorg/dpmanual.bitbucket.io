/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

float radius = 50;
int sw = 14;

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  noFill();
  stroke(255);
  
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
    background(0);
    strokeWeight(sw);
    float angle = map(mouseY, 0, height, 0, 360);
    
    int num = (int)map(mouseX, 0, width, 1, 60);
    for(int i=0; i<num; i++){
      drawForm(width/2, height/2, 3, radius*i, angle*(i*0.05));
    }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawForm(float _x, float _y, int _num, float _rad, float _angle) {
  pushMatrix();
  translate(_x, _y);
  rotate(radians(_angle));
  float angle = radians(360/_num); 
  beginShape();
  for (int i = 0; i<_num; i++) {

    float x = cos(i * angle) * _rad;
    float y = sin(i * angle) * _rad;
    
    vertex(x, y);
  }
  endShape(CLOSE);
  popMatrix();
}


void keyPressed() {
  if (key == 's') saveFrame("savedImage_###.png");
  if(key == '+'){
    radius+=5;
  }
   if(key == '-'){
    radius-=5;
  }
  if (key == 'w') sw +=2;
  if (key == 'x') {
    if(sw>0){
    sw -=2;
    }
  }
}