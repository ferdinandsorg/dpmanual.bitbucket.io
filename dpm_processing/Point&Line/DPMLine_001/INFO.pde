/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMLine_001
 * Parent Sketch: DPMLine_001
 * Type: auto
 *
 * Summary :  A Line
 *
 * GIT:
 * Author: mark webster 2019
 * https://dpmanual.bitbucket.io/
 */