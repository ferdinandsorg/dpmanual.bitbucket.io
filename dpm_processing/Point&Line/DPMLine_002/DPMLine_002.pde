/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
int seed;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 740);
  background(0);
  smooth();
  strokeCap(SQUARE);
  seed = (int)random(1000);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  randomSeed(seed);
  stroke(255);
  int sw = (int)map(mouseX, 0, width, 1, 100);
  strokeWeight(sw+1);
  for (int x=0; x <width; x+=sw) {
    float rndHeight = random(height);
    line(x, 0, x, height-rndHeight);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 'r') { 
    seed = (int)random(1000);
  }
  if (key =='s') {
    saveFrame("capture_###.png");
  }
}