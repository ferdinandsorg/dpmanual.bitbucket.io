/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMLine_002
 * Parent Sketch: none
 * Type: static
 *
 * Summary : Vertical repetition of lines with random length 
 *
 * GIT:
 * Author: mark webster 2019
 * https://dpmanual.bitbucket.io/
 */