/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////

int gridStep = 55; // gap between each
int lineLen = 50; // length of lines
int lineDensity = 10; // number of repetitions
float strokeWidth = 1.5;
int seed;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 720);
  background(0);
  smooth();
  stroke(255);
  strokeCap(SQUARE);
  seed = (int)random(1000);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  randomSeed(seed);
  strokeWeight(strokeWidth);
  
  for (int y=0; y<=height; y+=gridStep) {
    for (int x=0; x<=width; x+=gridStep) {
      int randVal = (int)random(0, 3);    
      if (randVal == 0) {
        drawLines(x, y, lineLen, true);
      }
      if (randVal == 1) {
        drawLines(x, y, lineLen, false);
      }
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawLines(int _x, int _y, int _len, boolean _isVertical) {
  int inter = gridStep/lineDensity;
  for (int i=0; i<gridStep; i+=inter) {
    if(_isVertical){
     line(_x+i, _y-_len, _x+i, _y+_len);
    }else {
      line(_x-_len, _y+i, _x+_len, _y+i);  
    }
  }
}

void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
  if(key == 'r'){
   seed = (int)random(1000); 
  }
}