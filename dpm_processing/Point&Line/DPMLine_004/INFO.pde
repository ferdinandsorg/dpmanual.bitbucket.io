/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMLine_004
 * Parent Sketch: None
 * Type: static
 *
 * Summary : Lines on a Circle
 *
 * GIT:
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */