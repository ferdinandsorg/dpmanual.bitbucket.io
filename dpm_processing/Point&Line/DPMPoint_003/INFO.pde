/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMPoint_003
 * Parent Sketch: DPMPoint_001
 * Type: auto
 *
 * Summary :  Random point iteration
 *
 * GIT:
 * Author: mark webster 2019
 * https://dpmanual.bitbucket.io/
 */