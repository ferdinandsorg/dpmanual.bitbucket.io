/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMText_001
 * Parent Sketch: None
 * Type: static
 *
 * Summary : Repeating Text
 *
 * GIT:
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */