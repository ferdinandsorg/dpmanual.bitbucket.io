/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


PFont f;
int seed;
int charSize = 24;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  f = createFont("IBMPlexMono-Bold", 12);
  textFont(f);
  seed = (int)random(1000);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0); 
  randomSeed(seed);
  drawLetters();
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawLetters() {
  textSize(charSize);
  int xMax = width/charSize;
  int yMax = height/charSize;

  for (int y=2; y<yMax; y++) {
    for (int x=1; x<xMax; x++) {
      char c = (char)random(1+mouseX, 1+mouseY);
      int xx = x * charSize;
      int yy = y * charSize;
      text(c, xx, yy);
    }
  }
}

void keyPressed() {
  if (key == 's') {
    saveFrame("export_###.png");
  }
  if (key =='+') {
    charSize++;
  }

  if (key =='-') {
    charSize--;
  }
}