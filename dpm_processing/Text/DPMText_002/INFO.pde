/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMText_002
 * Parent Sketch: none
 * Type: static
 *
 * Summary : Random glyphs on a grid
 *
 * GIT:
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */