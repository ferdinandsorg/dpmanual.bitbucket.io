/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


PFont f;
int seed;
boolean isSimpleRand = true;
int charSize = 18;
int glyphGridSize = 8;
int gridGap = charSize;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  f = createFont("IBMPlexMono-Bold", 120);
  textFont(f);
  seed = (int)random(1000);
}

/////////////////////////// DRAW ////////////////////////////

void draw() {
  background(0); 
  randomSeed(seed);
  int interval = charSize * glyphGridSize + gridGap;
  for (int y=20; y<height-15; y+=interval) {
    for (int x=15; x<width-15; x+=interval) {
      drawLetters(x, y, glyphGridSize);
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////

void drawLetters(float _x, float _y, int _num) {

  textSize(charSize);
  pushMatrix();
  translate(_x, _y);
  int randMin = (int)map(mouseX, 0, 500, 15, 65);
  int randMax = (int)map(mouseY, 0, 500, 66, 165);
  char c = ' ';
  if (isSimpleRand) {
    c = (char)random(randMin, randMax);
  }
  for (int y=0; y<_num; y++) {
    for (int x=0; x<_num; x++) {
      if (!isSimpleRand) {
        c = (char)random(randMin, randMax);
      }
      int xx = x * charSize;
      int yy = y * charSize;
      text(c, xx, yy);
    }
  }
  popMatrix();
}

void keyPressed() {
  if (key == 'r') {
    seed = (int)random(1000);
  }
    if (key == 'a') {
    isSimpleRand = !isSimpleRand;
  }
  if (key == 's') {
    saveFrame("export_###.png");
  }
  if (key =='+') {
    charSize++;
  }

  if (key =='-') {
    if (charSize>1) {
      charSize--;
    }
  }
  if (key =='w') {
    gridGap++;
  }

  if (key =='x') {
    if (gridGap>0) {
      gridGap--;
    }
  }
}