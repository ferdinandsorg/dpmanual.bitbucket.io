/*
 * :::::::::::::::::::::::::::::::::::
 * COMPUTATIONAL GRAPHIC DESIGN MANUAL
 * :::::::::::::::::::::::::::::::::::
 *
 * Sketch: DPMTexture_001
 * Parent Sketch: none
 * Type: dynamic
 *
 * Summary : Overlapping grid structures
 *
 * GIT:
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */