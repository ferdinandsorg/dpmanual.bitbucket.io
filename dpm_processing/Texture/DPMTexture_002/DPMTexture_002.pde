/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

int interval = 10;
int seed;
int sw = 2;
/////////////////////////// GLOBALS ////////////////////////////
void setup() {
  size(500,500);
  strokeCap(SQUARE);
  seed = (int)random(1000);
}

void draw(){
  randomSeed(seed);
   background(0);
  strokeWeight(sw);

  for(int x = 0; x<width-30; x+=100) {
    for(int y = 0; y<height-30; y+=100) { 
      form(x, y, 100, 0);
      form(x-10, y,100, 1);
      form(x, y, 100, 2);
    }
  }}


void form(int _x, int _y, int _size, int _type) {
  stroke(random(255));
  pushMatrix();
  translate(_x, _y);
  for(int i = 0; i < _size; i+=interval) {
      int j = _size-i;
      
      if(_type==0){
        line(i, 0, _size, j);
      }
        else if(_type==1){
        line(10, i, j+10, _size);
      }
        else if(_type==2){
        line(i, _size,_size, i);
      }
  }  
  popMatrix();
}


void keyPressed(){
  if(key == 'r'){
   seed = (int)random(1000); 
  }
  if(key == '+'){
   interval++;
  }
    if(key == '-'){
   interval--;
  }
    if(key == 'w'){
   sw++;
  }
    if(key == 'x'){
   sw--;
  }
}