/**
 * Random Points
 * 
 */


let randSeed = 1;
/////////////////////////// SETUP ////////////////////////////

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  background(0);
  strokeCap(SQUARE);
  randSeed = random(1000);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  randomSeed(randSeed);
  stroke(255);
  
  let sw = map(mouseX, 0, width, 1, 100);
  strokeWeight(1+sw);
  if(sw>1)
  for (let x=0; x <width; x+=sw) {
    let rndHeight = random(height);
    line(x, 0, x, height-rndHeight);
  }

}

/////////////////////////// FUNCTIONS ////////////////////////////
function keyPressed() {
    if (key == 'r') {
    randSeed = random(1000);
  }
}
