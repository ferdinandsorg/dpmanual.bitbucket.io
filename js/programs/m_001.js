/**
 * HARMONIC MOTION
 * Amandine
 * Sketch : oscillation_OffSet 
 */


function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    noStroke();
}

function draw() {
    background(0);     
    for (let x=40; x<width-30; x+=40 ) {
      let y = sin( x  + frameCount * 0.030) * 120; 
      let dia = sin( x  + frameCount * 0.10) * 90; // x+ décalage du mouvement en foncction de la valeur de x  
      
      ellipse(x, height/1.5 + y, dia, dia);
      ellipse(x, height/2 + y, dia/2, dia/2);
      ellipse(x, height/3 + y, dia/2, dia/2);
    }
}