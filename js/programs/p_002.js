/**
 * 
 * A grid of points
 */

let gridStep = 50;
let pntSize = 50;
let randSeed = 1;

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    background(0);
}

function draw() {
    background(0);
    randomSeed(randSeed);
    stroke(255);
    strokeWeight(pntSize);
    for (let y=pntSize; y<=height-pntSize; y+=gridStep) {
        for (let x=pntSize; x<=width-pntSize; x+=gridStep) {
            let randVal = random(12);
            let cutOffVal = map(mouseY, 0, height, 0, 12);
      if(randVal>cutOffVal){
        point(x, y);
      }
    }
  }
}

function keyPressed() {
    if(key === 'r') {randSeed = random(1000);}
    if(key === '+') {gridStep+=5; pntSize+=5;}
    if(key === '-') if(gridStep>5){gridStep-=5;  pntSize-=5;}
    if(key === 'w') pntSize +=5;
    if(key === 'x') if(pntSize>0){pntSize -=5;}
  }