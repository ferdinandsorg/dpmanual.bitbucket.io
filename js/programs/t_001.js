/**
 * Moiré
 */

/////////////////////////// GLOBALS ////////////////////////////
let interval = 10;
/////////////////////////// SETUP ////////////////////////////

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  background(0);
  smooth();
  noStroke();
  fill(255);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  translate(width/2, height/2);

  let dia2 = map(mouseY, 0, height, 1, 10);  
  drawGrid(dia2);
  rotate(mouseX*0.05);
  drawGrid(dia2);
}

/////////////////////////// FUNCTIONS ////////////////////////////

function drawGrid( _d) {
  for (let y=-height/2*2; y<=height-50; y+=interval) {
    for (let x=-width/2*2; x<=width-50; x+=interval) {
      ellipse(x, y, _d, _d);
    }
  }
}

function keyPressed() {
  if (key =='s') {
    //saveFrame("capture_###.png");
  }
   if (key =='+') {
    interval++;
  }
   if (key =='-') {
     interval--;
  }
}