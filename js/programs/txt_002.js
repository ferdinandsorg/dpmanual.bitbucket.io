/*
 * Repeating Text
 */

/////////////////////////// GLOBALS ////////////////////////////
let myFont;
let seed;
let charSize = 52;
/////////////////////////// SETUP ////////////////////////////

function preload() {
    myFont = loadFont('../font/IBMPlexMono-Bold.ttf');
}

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    background(0);
    seed = random(1000);
    textFont(myFont);
    textSize(charSize);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
    background(0);
    randomSeed(seed);
    fill(255);
    drawLetters();
}

/////////////////////////// FUNCTIONS ////////////////////////////
function drawLetters() {
    textFont(myFont);
    textSize(charSize);
    let xMax = 500 / charSize;
    let yMax = 500 / charSize;
    let glyphMin = map(mouseX, 0, 500, 0, 256);
    let glyphMax = map(mouseY, 0, 500, 0, 256);

    for (let y = 1; y < yMax; y++) {
        for (let x = 1; x < xMax-1; x++) {

            let c = char(random(1 + glyphMin, 1 + glyphMax));
            let xx = x * charSize;
            let yy = y * charSize;
            text(c, xx, yy);
        }
     }
}

function keyPressed() {
    if (key === 's') {
        //saveFrame("export_###.png");
    }
    if (key === '+') {
        charSize++;
    }

    if (key === '-') {
        charSize--;
    }
}