/*
 * Repeating Glyphs
 */


let myFont;
let seed;
let isSimpleRand = true;
let charSize = 18;
let glyphGridSize = 8;
let gridGap = charSize;
/////////////////////////// SETUP ////////////////////////////
function preload() {
    myFont = loadFont('../font/IBMPlexMono-Bold.ttf');
}

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  textFont(myFont);
  seed = random(1000);
}

/////////////////////////// DRAW ////////////////////////////

function draw() {
  background(0); 
  fill(255);
  randomSeed(seed);
  let interval = charSize * glyphGridSize + gridGap;
  for (let y=20; y<height-15; y+=interval) {
    for (let x=15; x<width-15; x+=interval) {
      drawLetters(x, y, glyphGridSize);
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////

function drawLetters( _x,  _y,  _num) {

  textSize(charSize);
  push();
  translate(_x, _y);
  let randMin = map(mouseX, 0, 500, 15, 65);
  let randMax = map(mouseY, 0, 500, 66, 165);
  let c = ' ';
  if (isSimpleRand) {
    c = char(random(randMin, randMax));
  }
  for (let y=0; y<_num; y++) {
    for (let x=0; x<_num; x++) {
      if (!isSimpleRand) {
        c = char(random(randMin, randMax));
      }
      let xx = x * charSize;
      let yy = y * charSize;
      text(c, xx, yy);
    }
  }
  pop();
}

function keyPressed() {
  if (key == 'r') {
    seed = random(1000);
  }
    if (key == 'a') {
    isSimpleRand = !isSimpleRand;
  }
  if (key == 's') {
    //saveFrame("export_###.png");
  }
  if (key =='+') {
    charSize++;
  }

  if (key =='-') {
    if (charSize>1) {
      charSize--;
    }
  }
  if (key =='w') {
    gridGap++;
  }

  if (key =='x') {
    if (gridGap>0) {
      gridGap--;
    }
  }
}
